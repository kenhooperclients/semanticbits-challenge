<?php

namespace Drupal\semanticbits_block\Plugin\Block;
use \Drupal\Core\Block\BlockBase;

/**
 * Custom block class to show today's pages
 * 
 * @Block(
 *  id = "semanticbits_block",
 *  admin_label = @Translation("SemanticBits Block"),
 *  category = @Translation("Custom Block")
 * )
 */
class SemanticBitsBlock extends BlockBase {
    /**
     * {@inheritdoc}
     */
    public function build() {
        return [
            '#markup' => $this->getCurrentPages(),
            '#cache' => [
                'max-age' => 0,
            ],
        ];
    }
    /**
     * Helper function to get today's pages
     */
    private function getCurrentPages() {
        $yesterday = strtotime('-1 day');
        $query = \Drupal::entityQuery('node')
            ->condition('changed', $yesterday, '>');
        $ids = $query->execute();
        $controller = \Drupal::entityManager()->getStorage('node');
        $entities = $controller->loadMultiple($ids);
        if ($entities != null) {
            $html = '<ul>';
            foreach ($entities as $entity) {
                $html .= '<li>';
                $html .= $entity->label();
                $html .= '</li>';
            }
            $html .= '</ul>';
        } else {
            $html = "<div><em>No pages were modified today.</em></div>";
        }
        
        return $html;
    }
}