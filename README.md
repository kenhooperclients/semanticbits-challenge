# SemanticBits PHP/Drupal Coding Challenge #

This is a small Drupal coding challenge that we normally ask all candidates to complete in order to help us better assess their coding abilities. The challenge is designed to hopefully not take more than 1-2 hours of your time (assuming you have previous experience with Drupal). The challenge is also designed to be aligned with the PHP/Drupal/DKAN-based project on which you most likely will be working on. The instructions are as follows.

Please develop a custom Drupal module that will render a list of pages that were modified on a current day:

Please develop a Block module
Ensure your module shows up in the Module Listing
Include a Drupal test case for your module i.e. Unit Tests.
Use Drupal 8 preferably.